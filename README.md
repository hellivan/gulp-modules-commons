# Collection of gulp-tasks for angular-fullstack client modules #

All the tasks from this collection can be registered by calling the register
function of the required module:

```
#!javascript

var tasks = require('gulp-client-commons');
tasks.register(gulp, bases, pkg);
```
The parameters for the register-function are all mandatory:
* **gulp**: The gulp-instance for which the tasks should be registered
* **bases**: Object that specifies the base-path of the module
* **pkg**: Either the required bower.json or package.json

Bases is an object of the following shape:

```
#!javascript

var bases = {
  app: './',   // folder that contains folders 'app' and 'components' (usually 'src' or './')
  dist: 'dist' // output-folder of the build-task 
};
```