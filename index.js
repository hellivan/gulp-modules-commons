'use strict';

var ClientStyles = require('./tasks/client/styles');
var ClientTemplates = require('./tasks/client/templates');
var ClientScripts = require('./tasks/client/scripts');
var ClientWatch = require('./tasks/client/watch');
var ClientBuild = require('./tasks/client/build');


var ServerScripts = require('./tasks/server/scripts');
var ServerWatch = require('./tasks/server/watch');
var ServerBuild = require('./tasks/server/build');
var ServerLocales = require('./tasks/server/locales');


var Clean = require('./tasks/common/clean');
var Serve = require('./tasks/common/serve');
var Build = require('./tasks/common/build');


function AbstractTasks(gulp, bases, packageFile, taskPrefix){
	this.gulp = gulp;
	this.bases = bases;
	this.packageFile = packageFile;
	this.taskPrefix = taskPrefix;

	Clean.register(gulp, bases);
}

AbstractTasks.prototype.registerServe = function(){
	Serve.register(this.gulp, this.taskPrefix);
};

AbstractTasks.prototype.registerBuild = function(){
	Build.register(this.gulp, this.taskPrefix);
};


function ClientTasks(gulp, bases, packageFile){
	AbstractTasks.call(this, gulp, bases, packageFile, 'client');

	ClientStyles.register(gulp, bases, packageFile);
	ClientTemplates.register(gulp, bases, packageFile);
	ClientScripts.register(gulp, bases, packageFile);
	ClientBuild.register(gulp, bases, packageFile);
	ClientWatch.register(gulp, bases, packageFile);
}
ClientTasks.prototype = AbstractTasks.prototype;


function ServerTasks(gulp, bases, packageFile){
	AbstractTasks.call(this, gulp, bases, packageFile, 'server');

	ServerScripts.register(gulp, bases, packageFile);
	ServerWatch.register(gulp, bases, packageFile);
	ServerBuild.register(gulp, bases, packageFile);
	ServerLocales.register(gulp, bases, packageFile);
}
ServerTasks.prototype = AbstractTasks.prototype;



module.exports.ClientTasks = ClientTasks;
module.exports.ServerTasks = ServerTasks;
