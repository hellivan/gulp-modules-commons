'use strict';

module.exports.register = function(gulp, bases, pkg){

    gulp.task('server.locales', function(){
        return gulp.src(bases.src + '/locales/**/*.json')
            .pipe(gulp.dest(bases.dist + '/locales'));
    });

};

