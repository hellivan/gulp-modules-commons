'use strict';

module.exports.register = function(gulp, bases, pkg){

    gulp.task('server.watch', function(){
        gulp.watch(bases.src + '/**/*.js', ['server.scripts']);
        gulp.watch(bases.src + '/locales/**/*.json', ['server.locales']);
    });

};
