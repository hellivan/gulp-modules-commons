'use strict';
var runSequence = require('run-sequence');

module.exports.register = function(gulp, bases, pkg){

	gulp.task('server.build', ['clean'], function(cb){
		runSequence.use(gulp)(['server.scripts', 'server.locales'], cb);
	});

};
