'use strict';

var babel = require('gulp-babel');
var sourceMaps = require('gulp-sourcemaps');

module.exports.register = function(gulp, bases, pkg){

    gulp.task('server.scripts', function(){
        return gulp.src('**/*.js', {cwd: bases.src})
            .pipe(sourceMaps.init())
            .pipe(babel())
            .pipe(sourceMaps.write())
            .pipe(gulp.dest(bases.dist));
    });

};

