'use strict';

module.exports.register = function(gulp, taskPrefix){

    var buildTask = 'build';
    if(taskPrefix){
        buildTask = taskPrefix + '.' + buildTask;
    }

    gulp.task('build', [buildTask])

};
