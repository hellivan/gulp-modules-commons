'use strict';

var del = require('del');

module.exports.register = function(gulp, bases){

	gulp.task('clean', function () {
		return del([bases.dist]);
	});
	
};

