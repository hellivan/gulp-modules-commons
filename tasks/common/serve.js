'use strict';
var runSequence = require('run-sequence');

module.exports.register = function(gulp, taskPrefix){

	var buildTask = 'build';
	var watchTask = 'watch';
	if(taskPrefix){
		buildTask = taskPrefix + '.' + buildTask;
		watchTask = taskPrefix + '.' + watchTask;
	}


	gulp.task('serve', [buildTask], function(cb){
		runSequence.use(gulp)(watchTask, cb);
	});

};
