'use strict';

module.exports.register = function(gulp, bases, pkg){

    gulp.task('client.watch', function(){
        // watch styles
        gulp.watch([
            bases.src + '/{app,components,styles}/**/*.{scss,css}',
            bases.src + '/app.css'
        ], ['client.styles']);
        
        // watch scripts
        gulp.watch([
            bases.src + '/{app,components,scripts}/**/*.js',
            bases.src + '/index.js'
        ], ['client.scripts']);
        
        // watch tempaltes
        gulp.watch([
            bases.src + '/{app,components,partials}/**/*.{jade,html}'
        ], ['client.templates']);

    });
    
};
