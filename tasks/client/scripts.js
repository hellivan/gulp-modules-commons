'use strict';

var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
var babel = require('gulp-babel');
var sourceMaps = require('gulp-sourcemaps');

module.exports.register = function (gulp, bases, pkg) {
    gulp.task('client.scripts', function () {
        return gulp.src([
                bases.src + '/index.js',
                bases.src + '/{app,components,scripts}/**/*.js',
                '!' + bases.src + '/{app,components,scripts}/**/*spec.js'])
            .pipe(sourceMaps.init())

            .pipe(babel())
            .pipe(ngAnnotate())
            .pipe(concat(pkg.name + ".js"))
            .pipe(sourceMaps.write())
            .pipe(gulp.dest(bases.dist));
    });
};
