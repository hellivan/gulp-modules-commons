'use strict';
var runSequence = require('run-sequence');

module.exports.register = function(gulp, bases, pkg){

	gulp.task('client.build', ['clean'], function(cb){
		runSequence.use(gulp)(['client.styles', 'client.templates', 'client.scripts'], cb);
	});

};
