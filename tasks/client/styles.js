'use strict';
var inject = require('gulp-inject');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var path = require('path');
var src = require('gulp-add-src');
var print = require('gulp-print');
var concat = require('gulp-concat');

module.exports.register = function(gulp, bases, pkg){
    var injectScss = function(){
        var sources = gulp.src(bases.src +'/{app,components,styles}/**/*.scss', {read: false});
        return inject(sources, {
            relative: true,
            starttag: '/* inject:imports */',
            endtag: '/* endinject */'
        });
    };
    
    var compileScss = function(){
        return sass({
            outputStyle: 'expanded',
            precision: 10,
            compass: false,
            loadPath: [
                path.join(bases.src, 'bower_components'),
                path.join(bases.src)
            ]
        }).on('error', sass.logError);
    };

    var addVendorPrefixes = function(){
        return autoprefixer({
            browsers: ['last 3 version'],
            expand: true
        });
    };

    gulp.task('client.styles', function () {
        return gulp.src(bases.src + '/app.scss')
            .pipe(injectScss())
            .pipe(compileScss())
            .pipe(src.prepend(bases.src + '/{app,components,styles}/**/*.css'))
            .pipe(addVendorPrefixes())
            .pipe(concat(pkg.name + ".css"))
            .pipe(gulp.dest(bases.dist));
    });
    
};
