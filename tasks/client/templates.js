'use strict';
var jade = require('gulp-jade');
var src = require('gulp-add-src');
var rename = require('gulp-rename');
var ngTemplates = require('gulp-ng-templates');


module.exports.register = function(gulp, bases, pkg){

	var templates = function(){
		return ngTemplates({
			standalone: false,
			module: pkg.name
		});
	};

	gulp.task('client.templates', function(){
		return gulp.src(bases.src +'/{app,components,partials}/**/*.jade')
			.pipe(jade())
			.pipe(src.append(bases.src +'/{app,components,partials}/**/*.html'))
			.pipe(templates())
			.pipe(rename(pkg.name + ".tpls.js"))
			.pipe(gulp.dest(bases.dist));
	});
	
};
